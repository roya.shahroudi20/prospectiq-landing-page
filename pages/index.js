import Header from "../components/Header";
import Navbar from "../components/Navbar";
import Hero from "../components/Hero";
import Quality from "../components/Quality";
import DataList from "../components/DataList";
import Services from "../components/Services";
import Contact from "../components/Contact";
import Accreditation from "../components/Accreditation";
import Footer from "../components/Footer";
import ClientSay from "../components/ClientSay";

export default function Home() {
  return (
    <div className="font-questrial overflow-hidden">
      <Header
        title="B2B Data | Prospect IQ | Access over 3.5m Contacts"
        description="Fully GDPR Compliant B2B Data - 90% Email and 98% Direct Mail Deliverability Warranty. Quality Business Data For Better Business Leads. Get A Quote Today."
      />
      <Navbar />
      <div className="bg-image">
        <Hero />
        <Quality />
        <DataList />
      </div>
      <Services />
      <ClientSay />
      <Contact />
      <Accreditation />
      <Footer />
    </div>
  );
}
