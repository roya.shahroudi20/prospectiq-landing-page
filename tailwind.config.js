module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        md: "2rem",
      },
    },
    extend: {
      colors: {
        purple: "#877BFB",
        "purple-light": "#F9F9FF",
        "purple-transparent": "#877BFB1F",
        "purple-dark": "#6f65cf",
        input: "#02163910",
        "input-focus": "#0274be",
        "blue-dark": "#021639",
      },
      fontFamily: {
        questrial: ["Questrial", "sans-serif"],
        montserrat: ["Montserrat", "sans-serif"],
      },
    },
  },
  variants: {
    extend: {
      borderWidth: ["hover", "focus"],
      margin: ["hover", "focus"],
    },
  },
  plugins: [],
};
