import ClientSlider from "./ClientSlider";

const ClientSay = () => {
  return (
    <section className="bg-blue-dark py-24">
      <div className="container">
        <ClientSlider />
      </div>
    </section>
  );
};

export default ClientSay;
