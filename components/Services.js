import Image from "next/image";
import ServicesSlider from "./ServicesSlider";

const Services = () => {
  return (
    <section className="py-20">
      <ul className="hidden md:container md:grid md:grid-cols-3 md:grid-rows-2 md:gap-16">
        <ServiceItem
          image="Scan.svg"
          title="Target Audience"
          description="We will look to understand who your ideal customers are to ensure
          your data is laser targeted and relevant"
        />
        <ServiceItem
          image="2-User.svg"
          title="Job Roles"
          description="We will look to understand the job titles your decision makers may
          carry to ensure you can contact the relevant individuals"
        />
        <ServiceItem
          image="Flower2.svg"
          title="Sectors"
          description="With access to over 3.5m records which is regularly cleansed and
          updated, you can target a wide range of industries with legitimate
          interest"
        />
        <ServiceItem
          image="Call-1.svg"
          title="Phone Numbers"
          description="All phone numbers are TPS & CTPS checked regularly to ensure you
          can pick up the phone with confidence to your target audience"
        />
        <ServiceItem
          image="Chat4-1.svg"
          title="Email Addresses"
          description="Contact decision makers for your products or services by
          contacting your prospects work email address"
        />
        <ServiceItem
          image="Message-1.svg"
          title="Direct mail data"
          description="Our business data list have the volume and quality you need to get
          your direct mail campaigns to the right people"
        />
      </ul>
      <div className="container md:hidden">
        <ServicesSlider />
      </div>
    </section>
  );
};

export default Services;

const ServiceItem = ({ image, title, description }) => {
  return (
    <li>
      <div className="flex flex-wrap items-center">
        <div className="p-7 bg-purple-transparent rounded-lg flex justify-center items-center mr-8 mb-5">
          <Image
            src={`/images/${image}`}
            width="44"
            height="44"
            layout="fixed"
          />
        </div>
        <h2 className="text-xl text-gray-900">{title}</h2>
      </div>
      <div className="mt-6">
        <p className="font-montserrat text-gray-800 leading-relaxed">
          {description}
        </p>
      </div>
    </li>
  );
};
