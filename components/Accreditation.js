import Image from "next/image";

const Accreditation = () => {
  return (
    <section className="bg-purple-transparent py-32">
      <div className="container grid md:grid-cols-2 gap-4">
        <div className="flex justify-center items-center order-2 md:order-1">
          <Image
            src="/images/accreditations-gdpr.png"
            width="250"
            height="160"
            layout="fixed"
          />
        </div>
        <div className="flex justify-center items-center order-1 md:order-2">
          <h2 className="text-4xl">Our Accreditations</h2>
        </div>
      </div>
    </section>
  );
};

export default Accreditation;
