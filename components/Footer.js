import Image from "next/image";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className="bg-blue-dark py-16 sm:py-28">
      <div className="container text-white md:relative">
        <div className="flex items-center">
          <Image
            src="/images/footer-logo.png"
            width="191"
            height="59"
            layout="fixed"
          />
        </div>
        <div className="hidden md:block md:absolute md:right-0 md:-mt-80">
          <Image
            src="/images/img-4.png"
            width="235"
            height="331"
            layout="fixed"
          />
        </div>
        <div className="md:grid grid-cols-1 gap-4 md:grid-cols-6">
          <div className="mb-8 md:mb-0">
            <p className="text-gray-400 pr-10 md:leading-loose md:text-lg">
              Prospect IQ provides quality B2B contact data to help you reach
              your next customers.
            </p>
          </div>
          <div className="col-span-2 grid grid-cols-2 gap-4 leading-loose mb-8 md:mb-0">
            <div>
              <ul>
                <li>
                  <Link href="#">
                    <a className="hover:text-gray-400 focus:text-gray-400 active:text-gray-400 outline-none">
                      Business Data
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="#">
                    <a className="hover:text-gray-400 focus:text-gray-400 active:text-gray-400 outline-none">
                      About us
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="#">
                    <a className="hover:text-gray-400 focus:text-gray-400 active:text-gray-400 outline-none">
                      Resources
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <div>
              <ul>
                <li>
                  <Link href="#">
                    <a className="hover:text-gray-400 focus:text-gray-400 active:text-gray-400 outline-none">
                      Contact Us
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="#">
                    <a className="hover:text-gray-400 focus:text-gray-400 active:text-gray-400 outline-none">
                      Get a Quote
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="#">
                    <a className="hover:text-gray-400 focus:text-gray-400 active:text-gray-400 outline-none">
                      Privacy Policy
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="mb-8 md:mb-0">
            <h3 className="pb-4">Social Media</h3>
            <ul className="flex flex-wrap space-x-2 items-center">
              <li>
                <Link href="#">
                  <a className="mb-4 flex justify-center items-center w-9 h-9 text-white border border-white rounded-lg hover:bg-white hover:text-blue-dark hover:-mt-3 focus:bg-white focus:text-blue-dark focus:-mt-3 outline-none transition-300">
                    <i className="fab fa-facebook-f"></i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="#">
                  <a className="mb-4 flex justify-center items-center w-9 h-9 text-white border border-white rounded-lg hover:bg-white hover:text-blue-dark hover:-mt-3 focus:bg-white focus:text-blue-dark focus:-mt-3 outline-none transition-300">
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="#">
                  <a className="mb-4 flex justify-center items-center w-9 h-9 text-white border border-white rounded-lg hover:bg-white hover:text-blue-dark hover:-mt-3 focus:bg-white focus:text-blue-dark focus:-mt-3 outline-none transition-300">
                    <i className="fab fa-instagram"></i>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="col-start-6 text-xs w-1/2 md:w-full">
            <p className="mb-4">
              Prospect IQ is a trading name of Mitchell and Stones Limited, a
              company registered in the United Kingdom, with registered number
              11650707 and with its registered office at Unit 1, Millbrook Road
              East, Southampton, Hampshire, United Kingdom, SO15 1JR
            </p>
            <p className="mb-4">ICO Registered: CSN8105699</p>
            <p>
              Copyright © 2020 Mitchell and Stones Limited. All rights reserved.
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
