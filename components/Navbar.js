import Image from "next/image";
import Link from "next/link";

const Navbar = () => {
  return (
    <header className="bg-white py-5 w-full fixed top-0 z-50">
      <div className="container flex justify-between items-center">
        <div className="flex items-center">
          <Image
            src="/images/logo.png"
            width="191"
            height="59"
            layout="fixed"
          />
        </div>
        <div className="text-3xl text-purple lg:hidden">
          <i className="fas fa-bars"></i>
        </div>
        <div className="hidden lg:block">
          <nav>
            <ul className="flex">
              <li className="mr-5">
                <Link href="#">
                  <a className="hover:text-purple focus:text-purple outline-none transition-300">
                    Business Data
                  </a>
                </Link>
              </li>
              <li className="mr-5">
                <Link href="#">
                  <a className="hover:text-purple focus:text-purple outline-none transition-300">About Us</a>
                </Link>
              </li>
              <li className="">
                <Link href="#">
                  <a className="hover:text-purple focus:text-purple outline-none transition-300">Resources</a>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
        <div className="hidden lg:block">
          <button className="mr-5 px-8 py-4 border border-blue-dark rounded-lg text-blue-dark hover:bg-blue-dark hover:text-white focus:bg-blue-dark focus:text-white focus:outline-none active:outline-none transition-300">
            Contact Us
          </button>
          <button className="px-5 py-4 border bg-blue-dark text-white focus:outline-none active:outline-none rounded-lg">
            Get a Quote
          </button>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
