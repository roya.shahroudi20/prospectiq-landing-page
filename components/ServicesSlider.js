import React, { Swiper, SwiperSlide } from "swiper/react";

import SwiperCore, { Autoplay, Navigation } from "swiper/core";
import Image from "next/image";

SwiperCore.use([Autoplay, Navigation]);

function ServicesSlider() {
  return (
    <>
      <Swiper
        navigation={true}
        autoplay={{
          delay: 5000,
        }}
        className="services-slider"
      >
        <SwiperSlide>
          <ServiceSlide
            image="Scan.svg"
            title="Target Audience"
            description="We will look to understand who your ideal customers are to ensure
          your data is laser targeted and relevant"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ServiceSlide
            image="2-User.svg"
            title="Job Roles"
            description="We will look to understand the job titles your decision makers may
          carry to ensure you can contact the relevant individuals"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ServiceSlide
            image="Flower2.svg"
            title="Sectors"
            description="With access to over 3.5m records which is regularly cleansed and
          updated, you can target a wide range of industries with legitimate
          interest"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ServiceSlide
            image="Call-1.svg"
            title="Phone Numbers"
            description="All phone numbers are TPS & CTPS checked regularly to ensure you
          can pick up the phone with confidence to your target audience"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ServiceSlide
            image="Chat4-1.svg"
            title="Email Addresses"
            description="Contact decision makers for your products or services by
          contacting your prospects work email address"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ServiceSlide
            image="Message-1.svg"
            title="Direct mail data"
            description="Our business data list have the volume and quality you need to get
          your direct mail campaigns to the right people"
          />
        </SwiperSlide>
      </Swiper>
    </>
  );
}
export default ServicesSlider;

const ServiceSlide = ({ image, title, description }) => {
  return (
    <div className="px-16">
      <div className="p-6 sm:p-14 flex-col justify-center items-center border border-gray-200">
        <div className="flex justify-center items-center">
          <div className="p-7 bg-purple-transparent rounded-lg flex justify-center items-center">
            <Image
              src={`/images/${image}`}
              width="44"
              height="44"
              layout="fixed"
            />
          </div>
        </div>
        <h2 className="my-5 text-lg text-center text-gray-900">{title}</h2>
        <p className="font-montserrat text-sm text-center text-gray-800 leading-relaxed">
          {description}
        </p>
      </div>
    </div>
  );
};
