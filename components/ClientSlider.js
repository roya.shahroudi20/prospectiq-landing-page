import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import SwiperCore, { Autoplay, Pagination, Navigation } from "swiper/core";
import Image from "next/image";

SwiperCore.use([Autoplay, Pagination, Navigation]);

function ClientSlider() {
  return (
    <>
      <div>
        <h2 className="md:-mb-20 text-4xl text-white">Client say about us</h2>
      </div>
      <Swiper
        slidesPerView={1}
        spaceBetween={50}
        autoplay={{
          delay: 6000,
        }}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        className="client-slider py-40 relative overflow-visible overflow-x-hidden"
      >
        <SwiperSlide>
          <ClientSlide
            title="It is clear they are experts"
            description="We have always been skeptical about purchasing data lists with
          concerns around quality and compliance with GDPR. Since engaging
          with Prospect IQ we found them to be very helpful and it is clear
          they are experts in their field. We also use their email marketing
          service and we were in talks with prospective customers from the
          first campaign!"
            name="Mike Guest"
            position="Managing Director, Sandler Training - Silverstone"
            image="mike-guest.png"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ClientSlide
            title="Highly recommend!"
            description="We have been using Prospect IQ for a while now. We find the data is accurate and the response times for counts and production are very efficient. They have now helped us on many campaigns and we will continue to use them. Great service from the team to top it all off. Highly recommend!"
            name="Kaushal Patel"
            position="Managing Director - Your Lead Machine"
            image="Kaushal-Patel.png"
          />
        </SwiperSlide>
        <SwiperSlide>
          <ClientSlide
            title="The results were excellent"
            description="We, as a company, have been struggling to find a high quality supplier of data for years. After speaking to Josh and his team we undertook a test of the data supplied. The results were excellent – far exceeding our expectations. I now look forward to working with the team as our preferred data supplier for our client list, which encompasses companies in a wide variety of sectors. Prospect IQ has been great to work with; professional, efficient and their data has been of the highest quality. A real ‘find’. Thanks team"
            name="Shane O'Byrne"
            position="Managing Director, Brilliant Blu Limited"
            image="shane.png"
          />
        </SwiperSlide>
      </Swiper>
    </>
  );
}
export default ClientSlider;

const ClientSlide = ({ title, description, name, position, image }) => {
  return (
    <div className="bg-blue-dark text-white grid grid-cols-1 gap-8 md:grid-cols-2">
      <div className="md:order-2">
        <Image
          src={`/images/${image}`}
          width="491"
          height="487"
          layout="responsive"
        />
      </div>
      <div className="text-left">
        <div>
          <h2 className="text-4xl mb-6">"{title}"</h2>
          <p className="text-xl text-gray-200">“{description}”</p>
        </div>
        <div className="mt-8">
          <h3 className="text-3xl">{name}</h3>
          <p className="text-xl w-1/2">{position}</p>
        </div>
      </div>
    </div>
  );
};
