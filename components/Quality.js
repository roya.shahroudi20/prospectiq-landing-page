import Image from "next/image";

const Quality = () => {
  return (
    <section className="py-28 overflow-hidden">
      <div className="relative container grid grid-cols-1 gap-8 md:grid-cols-9">
        <div className=" md:col-span-5">
          <div className="absolute -top-10 right-0 md:inset-x-1/2">
            <Image
              src="/images/Mask-Group-1.png"
              width="150"
              height="150"
              layout="fixed"
            />
          </div>
          <Image
            src="/images/video-thumbnail.png"
            width="500"
            height="330"
            layout="responsive"
          />
          <div className="absolute top-40 left-20 md:top-80 lg:left-48">
            <Image
              src="/images/How-it-works-2-300x93.png"
              width="150"
              height="43"
              layout="fixed"
            />
          </div>
          <div className="absolute top-56 left-36 sm:top-96 md:left-60 lg:left-96">
            <Image
              src="/images/Group-17.png"
              width="180"
              height="71"
              layout="fixed"
            />
          </div>
          <div className="absolute -top-10 -left-5">
            <Image
              src="/images/Group-14.png"
              width="100"
              height="126"
              layout="fixed"
            />
          </div>
          <div className="absolute -top-12 left-28 md:-top-16">
            <Image
              src="/images/Group-177.png"
              width="40"
              height="40"
              layout="fixed"
            />
          </div>
        </div>
        <div className="md:col-span-4 md:pl-3">
          <h2 className="m-8 text-4xl">Quality & Compliant Data</h2>
          <ul>
            <ListItem title="Fully GDPR Compliant" />
            <ListItem title="Accurate contactable data via Telephone, Email and Direct Mail" />
            <ListItem title="90% Email and 98% Direct Mail Deliveribility Warranty" />
            <ListItem title="Reach your next customers with confidence" />
            <ListItem title="Get advice from our data experts" />
          </ul>
        </div>
      </div>
    </section>
  );
};

export default Quality;

const ListItem = ({ title }) => {
  return (
    <li className="mb-8 flex items-center">
      <span className="mr-7">
        <Image
          src="/images/quality-icon.svg"
          width="20"
          height="20"
          layout="fixed"
        />
      </span>
      <span className="font-montserrat text-gray-500 font-light">{title}</span>
    </li>
  );
};
